all: compile clean

compile:
	pdflatex eurocv
	pdflatex eurocv
	pdflatex eurocv_mini
	pdflatex eurocv_mini

clean:
	rm -f *.aux *.toc *.log *~ *.backup *.out *.dvi *.lof
